/*
Name: Christopher Hurst
Class: CSC240
Due Date: 03/13/17
Files: Simple_Calculator.class, Simple_Calculator.java
Description: This program prompts the user to enter two number and then asks the 
    user if they want to add, subtract, multiply, or divide. The program once 
    commanded will give the answer to the user.
 */
package simple_calculator;

import java.util.Scanner;
public class Simple_Calculator {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        /* greets the user*/
        System.out.println("Welcome to the Simple Calculator");
        System.out.println("We will now perform a simple calculation: ");
        /* sets up the user input for the numbers */
        System.out.print("Enter your first number: ");
            double n1 = input.nextDouble();
        System.out.print("Enter your second number: ");
            double n2 = input.nextDouble();
        /* set up for what opperater the user wants */
        System.out.println("Tell me what operation you want to perform, Enter: ");
        System.out.println("    a - for addition");
        System.out.println("    s - for subtraction");
        System.out.println("    m - for multiplication");
        System.out.println("    d - for division");
        System.out.print("What is your choice: ");
            String symbol = input.next();
        /* takes var symbol and uses a switch to figure out the anser */
        switch (symbol)  {
            case "a":
                System.out.println("Your answer is " + (n1 + n2));
                break;
            case "s":
                System.out.println("Your answer is " + (n1 - n2));
                break;
            case "m":
                System.out.println("Your answer is " + (n1 * n2));
                break;
            case "d":
                System.out.println("Your asnwer is " + (int)((n1 / n2)*100)/100.0);
                break;
            default:
                System.out.println(symbol + " was not a valid input.");           
        }
        System.out.println("Thank you for using the Simple Calculator. Goodbye.");           
    }
}
